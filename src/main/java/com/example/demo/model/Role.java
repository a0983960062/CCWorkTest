package com.example.demo.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_REGISTER,
    ROLE_TRUSTED_CLIENT,
    ROLE_CLIENT
}
