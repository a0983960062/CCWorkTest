package com.example.demo.repository;

import com.example.demo.domain.EventToGuest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToGuestRespository extends JpaRepository<EventToGuest, Integer> {
}
