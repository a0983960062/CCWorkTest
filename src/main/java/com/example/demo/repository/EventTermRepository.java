package com.example.demo.repository;

import com.example.demo.domain.EventTerm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventTermRepository extends JpaRepository<EventTerm, Integer> {
}
