package com.example.demo.repository;

import com.example.demo.domain.EventToObject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToObjectRepository extends JpaRepository<EventToObject, Integer> {
}
