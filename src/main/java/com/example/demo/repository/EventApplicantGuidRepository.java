package com.example.demo.repository;

import com.example.demo.domain.EventApplicantGuid;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventApplicantGuidRepository extends JpaRepository<EventApplicantGuid, Integer> {
    EventApplicantGuid findByGuid(String guid);
}
