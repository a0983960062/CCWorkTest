package com.example.demo.repository;

import com.example.demo.domain.WorldRegion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorldRegionRepository extends JpaRepository<WorldRegion, Integer> {
    WorldRegion findByName(String name);
}
