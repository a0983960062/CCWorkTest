package com.example.demo.repository;

import com.example.demo.domain.EventSignUp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventSignUpRepository extends JpaRepository<EventSignUp, Integer> {
}
