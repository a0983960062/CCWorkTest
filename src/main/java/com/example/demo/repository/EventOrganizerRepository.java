package com.example.demo.repository;

import com.example.demo.domain.EventOrganizer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventOrganizerRepository extends JpaRepository<EventOrganizer, Integer> {
    EventOrganizer findByName(String name);
}
