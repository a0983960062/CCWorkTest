package com.example.demo.repository;

import com.example.demo.domain.EventToType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToTypeRepository extends JpaRepository<EventToType, Integer> {
}
