package com.example.demo.repository;

import com.example.demo.domain.EventYear;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventYearRepository extends JpaRepository<EventYear, Integer> {
}
