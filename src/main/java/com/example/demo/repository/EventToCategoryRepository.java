package com.example.demo.repository;

import com.example.demo.domain.EventToCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToCategoryRepository extends JpaRepository<EventToCategory, Integer> {
}
