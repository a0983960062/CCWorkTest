package com.example.demo.repository;

import com.example.demo.domain.EventTypes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventTypesRepository extends JpaRepository<EventTypes, Integer> {
}
