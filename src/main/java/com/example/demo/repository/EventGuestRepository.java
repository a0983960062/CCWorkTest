package com.example.demo.repository;

import com.example.demo.domain.EventGuest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventGuestRepository extends JpaRepository<EventGuest, Integer> {
    EventGuest findByName(String name);
}
