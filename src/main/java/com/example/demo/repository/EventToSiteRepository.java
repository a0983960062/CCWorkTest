package com.example.demo.repository;

import com.example.demo.domain.EventToSite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToSiteRepository extends JpaRepository<EventToSite, Integer> {
}
