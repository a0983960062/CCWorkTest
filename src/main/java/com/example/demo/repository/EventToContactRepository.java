package com.example.demo.repository;

import com.example.demo.domain.EventToContact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToContactRepository extends JpaRepository<EventToContact, Integer> {
}
