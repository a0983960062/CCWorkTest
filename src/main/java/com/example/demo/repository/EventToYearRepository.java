package com.example.demo.repository;

import com.example.demo.domain.EventToYear;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventToYearRepository extends JpaRepository<EventToYear, Integer> {
}
