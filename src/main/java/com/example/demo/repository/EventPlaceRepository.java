package com.example.demo.repository;

import com.example.demo.domain.EventPlace;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventPlaceRepository extends JpaRepository<EventPlace, Integer> {
    EventPlace findByName(String name);
}
