package com.example.demo.repository;

import com.example.demo.domain.EventSignUpSendMailLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventSignUpSendMailLogRepository extends JpaRepository<EventSignUpSendMailLog, Integer> {
}
