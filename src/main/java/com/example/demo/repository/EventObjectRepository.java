package com.example.demo.repository;

import com.example.demo.domain.EventObject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventObjectRepository extends JpaRepository<EventObject, Integer> {
    EventObject findByName(String name);
}
