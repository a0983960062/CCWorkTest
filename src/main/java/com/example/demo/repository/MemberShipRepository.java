package com.example.demo.repository;

import com.example.demo.domain.MemberShip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberShipRepository extends JpaRepository<MemberShip, Integer> {
}