package com.example.demo.repository;

import com.example.demo.domain.Events;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventsRepository extends JpaRepository<Events, Integer> {
    Events findByTitle(String title);
}
