package com.example.demo.repository;

import com.example.demo.domain.EventContact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventContactRepository extends JpaRepository<EventContact, Integer> {
    EventContact findByName(String name);
}
