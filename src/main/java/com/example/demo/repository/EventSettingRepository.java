package com.example.demo.repository;

import com.example.demo.domain.EventSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventSettingRepository extends JpaRepository<EventSetting, Integer> {
}
