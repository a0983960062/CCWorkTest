package com.example.demo.repository;

import com.example.demo.domain.EventApplicant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventApplicantRepository extends JpaRepository<EventApplicant, Integer> {
    EventApplicant findByEventID(Integer id);
}
