package com.example.demo.repository;

import com.example.demo.domain.EventSet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventSetRepository extends JpaRepository<EventSet, Integer> {
    EventSet findByTypes(String types);
}
