package com.example.demo.resource;

import com.example.demo.domain.EventToGuest;
import com.example.demo.repository.EventToGuestRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToGuest")
public class EventToGuestResource {
    @Autowired
    EventToGuestRespository eventToGuestRespository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToGuest> getAll() {
        return eventToGuestRespository.findAll();
    }
}
