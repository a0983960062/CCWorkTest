package com.example.demo.resource;

import com.example.demo.domain.EventSignUp;
import com.example.demo.repository.EventSignUpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventSignUp")
public class EventSignUpResource {
    @Autowired
    EventSignUpRepository eventSignUpRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventSignUp> getAll() {
        return eventSignUpRepository.findAll();
    }
}
