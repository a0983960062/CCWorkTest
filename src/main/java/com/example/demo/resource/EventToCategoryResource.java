package com.example.demo.resource;

import com.example.demo.domain.EventToCategory;
import com.example.demo.repository.EventToCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToCategory")
public class EventToCategoryResource {
    @Autowired
    EventToCategoryRepository eventToCategoryRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToCategory> getAll() {
        return eventToCategoryRepository.findAll();
    }
}
