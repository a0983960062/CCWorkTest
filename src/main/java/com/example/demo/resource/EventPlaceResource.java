package com.example.demo.resource;

import com.example.demo.domain.EventPlace;
import com.example.demo.repository.EventPlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventPlace")
public class EventPlaceResource {
    @Autowired
    EventPlaceRepository eventPlaceRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventPlace> getAll() {
        return eventPlaceRepository.findAll();
    }
}
