package com.example.demo.resource;

import com.example.demo.domain.MemberShip;
import com.example.demo.repository.MemberShipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/memberShip")
public class MemberShipResource {
    @Autowired
    MemberShipRepository memberShipRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<MemberShip> getAll() {
        return memberShipRepository.findAll();
    }
}
