package com.example.demo.resource;

import com.example.demo.domain.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/product")
public class ProductResource {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/load")
    public ResponseEntity<Product> persist(@RequestParam(value = "name") String name, @RequestParam(value = "price") int price, @RequestParam(value = "description") String description) {
        Product product = new Product(name, price, description);
        productRepository.save(product);
        return new ResponseEntity ((MultiValueMap<String, String>) productRepository.findAll(), HttpStatus.OK);
    }
}
