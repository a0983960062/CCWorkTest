package com.example.demo.resource;

import com.example.demo.domain.EventToType;
import com.example.demo.repository.EventToTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToType")
public class EventToTypeResource {
    @Autowired
    EventToTypeRepository eventToTypeRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToType> getAll() {
        return eventToTypeRepository.findAll();
    }
}
