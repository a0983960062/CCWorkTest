package com.example.demo.resource;

import com.example.demo.domain.EventYear;
import com.example.demo.repository.EventYearRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventYear")
public class EventYearResource {
    @Autowired
    EventYearRepository eventYearRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventYear> getAll() {
        return eventYearRepository.findAll();
    }
}
