package com.example.demo.resource;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/user")
public class UserResource {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserService userService;

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable Integer id) {
        userRepository.delete(id);
        return new ResponseEntity<> (HttpStatus.OK);
    }

    @PreAuthorize("hasRole('REGISTER')")
    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public ResponseEntity<Object> persist(@RequestParam(value = "phone") String phone, @RequestParam(value = "username") String username, @RequestParam(value = "password") String password) {
        User user = new User(phone, username, password);
        userService.register(user);
        return new ResponseEntity<> (user, HttpStatus.OK);
    }

//    @PreAuthorize("hasRole('REGISTER')")
//    @RequestMapping(method = RequestMethod.POST, value = "/create")
//    public ResponseEntity<Object> persist(@RequestBody) {
//        User user = new User(phone, username, password);
//        userService.register(user);
//        return new ResponseEntity<> (user, HttpStatus.OK);
}
