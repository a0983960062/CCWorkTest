package com.example.demo.resource;

import com.example.demo.domain.EventOrganizer;
import com.example.demo.repository.EventOrganizerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventOrganizer")
public class EventOrganizerResource {
    @Autowired
    EventOrganizerRepository eventOrganizerRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventOrganizer> getAll() {
        return eventOrganizerRepository.findAll();
    }
}
