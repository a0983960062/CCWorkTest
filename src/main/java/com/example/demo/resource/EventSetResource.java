package com.example.demo.resource;

import com.example.demo.domain.EventSet;
import com.example.demo.repository.EventSetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventSet")
public class EventSetResource {
    @Autowired
    EventSetRepository eventSetRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventSet> getAll() {
        return eventSetRepository.findAll();
    }
}
