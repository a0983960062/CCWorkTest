package com.example.demo.resource;

import com.example.demo.domain.WorldRegion;
import com.example.demo.repository.WorldRegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/worldRegion")
public class WorldRegionResource {
    @Autowired
    WorldRegionRepository worldRegionRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<WorldRegion> getAll() {
        return worldRegionRepository.findAll();
    }
}
