package com.example.demo.resource;

import com.example.demo.domain.EventToSite;
import com.example.demo.repository.EventToSiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToSite")
public class EventToSiteResource {
    @Autowired
    EventToSiteRepository eventToSiteRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToSite> getAll() {
        return eventToSiteRepository.findAll();
    }
}
