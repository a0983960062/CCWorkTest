package com.example.demo.resource;

import com.example.demo.domain.Events;
import com.example.demo.repository.EventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/events")
public class EventsResource {
    @Autowired
    EventsRepository eventsRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Events> getAll() {
        return eventsRepository.findAll();
    }
}
