package com.example.demo.resource;

import com.example.demo.domain.EventTypes;
import com.example.demo.repository.EventTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventTypes")
public class EventTypesResource {
    @Autowired
    EventTypesRepository eventTypesRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventTypes> getAll() {
        return eventTypesRepository.findAll();
    }
}
