package com.example.demo.resource;

import com.example.demo.domain.EventTerm;
import com.example.demo.repository.EventTermRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventTerm")
public class EventTermResource {
    @Autowired
    EventTermRepository eventTermRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventTerm> getAll() {
        return eventTermRepository.findAll();
    }
}
