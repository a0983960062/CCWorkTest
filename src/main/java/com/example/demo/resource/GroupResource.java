package com.example.demo.resource;

import com.example.demo.domain.Group;
import com.example.demo.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/group")
public class GroupResource {
    @Autowired
    GroupRepository groupRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<Group> getAll() {
        return groupRepository.findAll();
    }
}
