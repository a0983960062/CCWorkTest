package com.example.demo.resource;

import com.example.demo.domain.EventToPlace;
import com.example.demo.repository.EventToPlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToPlace")
public class EventToPlaceResource {
    @Autowired
    EventToPlaceRepository eventToPlaceRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToPlace> getAll() {
        return eventToPlaceRepository.findAll();
    }
}
