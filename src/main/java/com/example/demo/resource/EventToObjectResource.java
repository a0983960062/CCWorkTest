package com.example.demo.resource;

import com.example.demo.domain.EventToObject;
import com.example.demo.repository.EventToObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToObject")
public class EventToObjectResource {
    @Autowired
    EventToObjectRepository eventToObjectRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToObject> getAll() {
        return eventToObjectRepository.findAll();
    }
}
