package com.example.demo.resource;

import com.example.demo.domain.EventContact;
import com.example.demo.repository.EventContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventContact")
public class EventContactResource {
    @Autowired
    EventContactRepository eventContactRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventContact> getAll() {
        return eventContactRepository.findAll();
    }
}
