package com.example.demo.resource;

import com.example.demo.domain.EventApplicantGuid;
import com.example.demo.repository.EventApplicantGuidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventApplicantGuid")
public class EventApplicantGuidResource {
    @Autowired
    EventApplicantGuidRepository eventApplicantGuidRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventApplicantGuid> getAll() {
        return eventApplicantGuidRepository.findAll();
    }
}
