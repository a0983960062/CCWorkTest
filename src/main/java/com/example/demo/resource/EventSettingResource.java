package com.example.demo.resource;

import com.example.demo.domain.EventSetting;
import com.example.demo.repository.EventSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventSetting")
public class EventSettingResource {
    @Autowired
    EventSettingRepository eventSettingRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventSetting> getAll() {
        return eventSettingRepository.findAll();
    }
}
