package com.example.demo.resource;

import com.example.demo.domain.EventToOrganizer;
import com.example.demo.repository.EventToOrganizerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToOrganizer")
public class EventToOrganizerResource {
    @Autowired
    EventToOrganizerRepository eventToOrganizerRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToOrganizer> getAll() {
        return eventToOrganizerRepository.findAll();
    }
}
