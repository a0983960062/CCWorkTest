package com.example.demo.resource;

import com.example.demo.domain.EventGuest;
import com.example.demo.repository.EventGuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventGuest")
public class EventGuestResource {
    @Autowired
    EventGuestRepository eventGuestRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventGuest> getAll() {
        return eventGuestRepository.findAll();
    }
}
