package com.example.demo.resource;

import com.example.demo.domain.EventObject;
import com.example.demo.repository.EventObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventObject")
public class EventObjectResource {
    @Autowired
    EventObjectRepository eventObjectRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventObject> getAll() {
        return eventObjectRepository.findAll();
    }
}
