package com.example.demo.resource;

import com.example.demo.domain.EventToYear;
import com.example.demo.repository.EventToYearRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventToYear")
public class EventToYearResource {
    @Autowired
    EventToYearRepository eventToYearRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventToYear> getAll() {
        return eventToYearRepository.findAll();
    }
}
