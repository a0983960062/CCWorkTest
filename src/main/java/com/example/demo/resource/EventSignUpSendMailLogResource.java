package com.example.demo.resource;

import com.example.demo.domain.EventSignUpSendMailLog;
import com.example.demo.repository.EventSignUpSendMailLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/eventSignUpSendMailLog")
public class EventSignUpSendMailLogResource {
    @Autowired
    EventSignUpSendMailLogRepository eventSignUpSendMailLogRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public List<EventSignUpSendMailLog> getAll() {
        return eventSignUpSendMailLogRepository.findAll();
    }
}
