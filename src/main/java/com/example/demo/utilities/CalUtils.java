package com.example.demo.utilities;

import java.util.Calendar;
import java.util.Locale;

public final class CalUtils {

    private static final Locale locale = Locale.getDefault();

    public static long getNow() {
        return Calendar.getInstance(locale).getTimeInMillis();
    }
}
