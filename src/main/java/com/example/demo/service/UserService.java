package com.example.demo.service;

import com.example.demo.domain.User;
import com.example.demo.model.Role;
import com.example.demo.repository.UserRepository;
import com.example.demo.utilities.CalUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private final Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = userRepository.findByUsername(username);

        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UsernameNotFoundException(String.format("Username[%s] not found", username));
        }
    }

    public User findAccountByUsername(String username) throws UsernameNotFoundException {

        Optional<User> account = userRepository.findByUsername(username);

        if (account.isPresent()) {
            return account.get();
        } else {
            throw new UsernameNotFoundException(String.format("Username[%s] not found", username));
        }
    }

    public User findUserByPhone(String phone) {
        Optional<User> account = userRepository.findByPhone(phone);
        return account.orElse(null);
    }

    /**
     * 資料註冊帳號
     *
     * @param user 帳號(填入username, password, phone)
     * @return 帳號資料
     */
    public User register(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.grantAuthority(Role.ROLE_USER);
        return userRepository.save(user);
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public User get(String phone, String validateCode) {

        // 查詢帳號是否存在
        User user = findUserByPhone(phone);

        // 不存在
        if (user == null) {
            user = createBySystem(phone);
            user.setPassword(validateCode);
            user = register(user);
        }
        // 存在
        else {
            // 更新密碼
            user.setPassword(validateCode);
            user = encodePasswordAndSave(user);
        }

        return user;
    }

    //系統註冊帳號
    private User createBySystem(String phone) {
        long now = CalUtils.getNow();
        String username = "User_" + now;
        return new User(phone, username, null);
    }

    // 更新密碼
    private User encodePasswordAndSave(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    // To successfully remove the date @Transactional annotation must be added
    @Transactional
    public boolean removeAuthenticatedAccount() throws UsernameNotFoundException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = findAccountByUsername(username);
        int del = userRepository.deleteAccountById(user.getId());
        return del > 0;
    }

}
