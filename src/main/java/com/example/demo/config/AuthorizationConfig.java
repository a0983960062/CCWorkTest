package com.example.demo.config;

import com.example.demo.model.Role;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {

    private int accessTokenValiditySeconds = 60 * 60 * 3;
    private int refreshTokenValiditySeconds = 60 * 60 * 24 * 30;
//
    @Value("${security.oauth2.resource.id}")
    private String resourceId;
//
//    private String ccEventResourceId = "ccEvent_id";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserService();
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // Authentication驗證方式，AuthenticationManager是一個Interface

        // 傳統的驗證方式
        //endpoints.authenticationManager(this.authenticationManager);

        // JWT的驗證方式
        endpoints
                .authenticationManager(this.authenticationManager)
                .tokenServices(tokenServices())
                .tokenStore(tokenStore())
                .accessTokenConverter(accessTokenConverter());
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .tokenKeyAccess("hasAuthority('ROLE_TRUSTED_CLIENT')")
                .checkTokenAccess("hasAuthority('ROLE_TRUSTED_CLIENT')");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                // Client Id
                .withClient("trusted-app")
                // 可使用OAuth2的流程模式
                .authorizedGrantTypes("client_credentials", "password", "refresh_token")
                // 權力，角色
                .authorities(Role.ROLE_TRUSTED_CLIENT.toString())
                // 權限(此為自訂)
                .scopes("read", "write")
                // 可使用的 Resource Server Id
                .resourceIds(resourceId)
                // Access Token時效
                .accessTokenValiditySeconds(accessTokenValiditySeconds)
                // Refresh Token時效
                .refreshTokenValiditySeconds(refreshTokenValiditySeconds)
                // Client Secret
                .secret("secret")

                // 帳號註冊
                .and()
                .withClient("register-app")
                .authorizedGrantTypes("client_credentials")
                .authorities(Role.ROLE_REGISTER.toString())
                .scopes("register")
                //.accessTokenValiditySeconds(10)
                //.refreshTokenValiditySeconds(10)
                .resourceIds(resourceId)
                .secret("secret");
    }

    //----------------------------------------------------------------------------------------
    // JSON Web Token
    //----------------------------------------------------------------------------------------

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        // Symmetric key (對稱)
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey("kidtech");
        return converter;

        // Asymmetric key (非對稱)
        // 鑰匙路徑：src/main/resources/***.jks
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
//                new ClassPathResource("kidtech.jks"),
//                "kidtech".toCharArray()
//        );
//        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("kidtech"));
//        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices services = new DefaultTokenServices();
        services.setTokenStore(tokenStore());
        services.setSupportRefreshToken(true);
        services.setTokenEnhancer(accessTokenConverter());
        // 更改JWT的expires_in(失效時間)
        services.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
        services.setRefreshTokenValiditySeconds(refreshTokenValiditySeconds);
        return services;
    }

}
