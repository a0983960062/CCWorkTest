package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventTerm")
public class EventTerm {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MenuID")
    private Integer menuID;

    @Column(name = "Contents")
    private String content;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    public EventTerm() {
    }

    public EventTerm(String content, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime) {
        this.content = content;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
    }

    public Integer getMenuID() {
        return menuID;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }
}
