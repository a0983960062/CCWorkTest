package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventSignUp")
public class EventSignUp {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "Type")
    private Integer type;

    @Column(name = "ButtonText")
    private String buttonText;

    @Column(name = "SignUpStart")
    private Timestamp signUpStart;

    @Column(name = "SignUpEnd")
    private Timestamp signUpEnd;

    @Column(name = "SignUpLink")
    private String signUpLink;

    @Column(name = "AdmissionMethod")
    private Integer admissionMethod;

    @Column(name = "ReservationQuota")
    private Integer reservationQuota;

    @Column(name = "AdmissionQuota")
    private Integer admissionQuota;

    @Column(name = "IsWaitingQuota")
    private Boolean isWaitingQuota;

    @Column(name = "WaitingQuota")
    private Integer waitingQuota;

    @Column(name = "IsReserveQuota")
    private Boolean isReserveQuota;

    @Column(name = "ReserveQuota")
    private Integer reserveQuota;

    @Column(name = "IsPublicRemainingQuota")
    private Boolean isPublicRemainingQuota;

    @Column(name = "RemainingText")
    private String remainingText;

    @Column(name = "IsMemberOnly")
    private Boolean isMemberOnly;

    @Column(name = "RepeatType")
    private Integer repeatType;

    @Column(name = "RepeatLimit")
    private Integer repeatLimit;

    @Column(name = "IsClientCancel")
    private Boolean isClientCancel;

    @Column(name = "ClientCancelDay")
    private Integer clientCancelDay;

    @Column(name = "ReportQRCode")
    private Boolean reportQRCode;

    @Column(name = "ReportWeb")
    private Boolean reportWeb;

    @Column(name = "SignUpLive")
    private Boolean signUpLive;

    @Column(name = "ReportField")
    private Long reportField;

    @Column(name = "IsReportConsiderations")
    private Boolean isReportConsiderations;

    @Column(name = "ReportConsiderations")
    private String reportConsiderations;

    @Column(name = "ReportBackground")
    private String reportBackground;

    @Column(name = "ReportStart")
    private Timestamp reportStart;

    @Column(name = "ReportEnd")
    private Timestamp reportEnd;

    @Column(name = "IsReportPlace")
    private Boolean isReportPlace;

    @Column(name = "ReportPlaceName")
    private String reportPlaceName;

    @Column(name = "ReportPlaceCountry")
    private Integer reportPlaceCountry;

    @Column(name = "ReportPlaceProvince")
    private Integer reportPlaceProvince;

    @Column(name = "ReportPlaceCity")
    private Integer reportPlaceCity;

    @Column(name = "ReportPlaceRegion")
    private Integer reportPlaceRegion;

    @Column(name = "ReportPlaceAddress")
    private String reportPlaceAddress;

    @Column(name = "IsClause")
    private Boolean isClause;

    @Column(name = "Clause")
    private String clause;

    @Column(name = "IsVerificationCode")
    private Boolean isVerificationCode;

    @Column(name = "IsNotify")
    private Boolean isNotify;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    @Column(name = "CardNo")
    private Long cardNo;

    @Column(name = "NotifyManagement")
    private String notifyManagement;

    @Column(name = "QuestionnairesType")
    private Integer questionnairesType;

    @Column(name = "QuestionnairesButtonText")
    private String questionnairesButtonText;

    @Column(name = "QuestionnairesStart")
    private Timestamp questionnairesStart;

    @Column(name = "QuestionnairesEnd")
    private Timestamp questionnairesEnd;

    @Column(name = "QuestionnairesUrl")
    private String questionnairesUrl;

    @Column(name = "Independent")
    private Boolean independent;

    @Column(name = "IndependentType")
    private Integer independentType;

    @Column(name = "IndependentLogo")
    private String independentLogo;

    @Column(name = "IndependentLogoM")
    private String independentLogoM;

    @Column(name = "IndependentImg")
    private String independentImg;

    @Column(name = "IndependentImgM")
    private String independentImgM;

    @Column(name = "QuestionnaireMenuID")
    private Long questionnaireMenuID;

    @Column(name = "QuestionnaireFormID")
    private Long questionnaireFormID;

    public EventSignUp() {
    }

    public EventSignUp(Integer type, String buttonText, Timestamp signUpStart, Timestamp signUpEnd, String signUpLink, Integer admissionMethod, Integer reservationQuota, Integer admissionQuota, Boolean isWaitingQuota, Integer waitingQuota, Boolean isReserveQuota, Integer reserveQuota, Boolean isPublicRemainingQuota, String remainingText, Boolean isMemberOnly, Integer repeatType, Integer repeatLimit, Boolean isClientCancel, Integer clientCancelDay, Boolean reportQRCode, Boolean reportWeb, Boolean signUpLive, Long reportField, Boolean isReportConsiderations, String reportConsiderations, String reportBackground, Timestamp reportStart, Timestamp reportEnd, Boolean isReportPlace, String reportPlaceName, Integer reportPlaceCountry, Integer reportPlaceProvince, Integer reportPlaceCity, Integer reportPlaceRegion, String reportPlaceAddress, Boolean isClause, String clause, Boolean isVerificationCode, Boolean isNotify, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime, Long cardNo, String notifyManagement, Integer questionnairesType, String questionnairesButtonText, Timestamp questionnairesStart, Timestamp questionnairesEnd, String questionnairesUrl, Boolean independent, Integer independentType, String independentLogo, String independentLogoM, String independentImg, String independentImgM, Long questionnaireMenuID, Long questionnaireFormID) {
        this.type = type;
        this.buttonText = buttonText;
        this.signUpStart = signUpStart;
        this.signUpEnd = signUpEnd;
        this.signUpLink = signUpLink;
        this.admissionMethod = admissionMethod;
        this.reservationQuota = reservationQuota;
        this.admissionQuota = admissionQuota;
        this.isWaitingQuota = isWaitingQuota;
        this.waitingQuota = waitingQuota;
        this.isReserveQuota = isReserveQuota;
        this.reserveQuota = reserveQuota;
        this.isPublicRemainingQuota = isPublicRemainingQuota;
        this.remainingText = remainingText;
        this.isMemberOnly = isMemberOnly;
        this.repeatType = repeatType;
        this.repeatLimit = repeatLimit;
        this.isClientCancel = isClientCancel;
        this.clientCancelDay = clientCancelDay;
        this.reportQRCode = reportQRCode;
        this.reportWeb = reportWeb;
        this.signUpLive = signUpLive;
        this.reportField = reportField;
        this.isReportConsiderations = isReportConsiderations;
        this.reportConsiderations = reportConsiderations;
        this.reportBackground = reportBackground;
        this.reportStart = reportStart;
        this.reportEnd = reportEnd;
        this.isReportPlace = isReportPlace;
        this.reportPlaceName = reportPlaceName;
        this.reportPlaceCountry = reportPlaceCountry;
        this.reportPlaceProvince = reportPlaceProvince;
        this.reportPlaceCity = reportPlaceCity;
        this.reportPlaceRegion = reportPlaceRegion;
        this.reportPlaceAddress = reportPlaceAddress;
        this.isClause = isClause;
        this.clause = clause;
        this.isVerificationCode = isVerificationCode;
        this.isNotify = isNotify;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
        this.cardNo = cardNo;
        this.notifyManagement = notifyManagement;
        this.questionnairesType = questionnairesType;
        this.questionnairesButtonText = questionnairesButtonText;
        this.questionnairesStart = questionnairesStart;
        this.questionnairesEnd = questionnairesEnd;
        this.questionnairesUrl = questionnairesUrl;
        this.independent = independent;
        this.independentType = independentType;
        this.independentLogo = independentLogo;
        this.independentLogoM = independentLogoM;
        this.independentImg = independentImg;
        this.independentImgM = independentImgM;
        this.questionnaireMenuID = questionnaireMenuID;
        this.questionnaireFormID = questionnaireFormID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public Timestamp getSignUpStart() {
        return signUpStart;
    }

    public void setSignUpStart(Timestamp signUpStart) {
        this.signUpStart = signUpStart;
    }

    public Timestamp getSignUpEnd() {
        return signUpEnd;
    }

    public void setSignUpEnd(Timestamp signUpEnd) {
        this.signUpEnd = signUpEnd;
    }

    public String getSignUpLink() {
        return signUpLink;
    }

    public void setSignUpLink(String signUpLink) {
        this.signUpLink = signUpLink;
    }

    public Integer getAdmissionMethod() {
        return admissionMethod;
    }

    public void setAdmissionMethod(Integer admissionMethod) {
        this.admissionMethod = admissionMethod;
    }

    public Integer getReservationQuota() {
        return reservationQuota;
    }

    public void setReservationQuota(Integer reservationQuota) {
        this.reservationQuota = reservationQuota;
    }

    public Integer getAdmissionQuota() {
        return admissionQuota;
    }

    public void setAdmissionQuota(Integer admissionQuota) {
        this.admissionQuota = admissionQuota;
    }

    public Boolean getWaitingQuota() {
        return isWaitingQuota;
    }

    public void setWaitingQuota(Integer waitingQuota) {
        this.waitingQuota = waitingQuota;
    }

    public Boolean getReserveQuota() {
        return isReserveQuota;
    }

    public void setReserveQuota(Integer reserveQuota) {
        this.reserveQuota = reserveQuota;
    }

    public Boolean getPublicRemainingQuota() {
        return isPublicRemainingQuota;
    }

    public void setPublicRemainingQuota(Boolean publicRemainingQuota) {
        isPublicRemainingQuota = publicRemainingQuota;
    }

    public String getRemainingText() {
        return remainingText;
    }

    public void setRemainingText(String remainingText) {
        this.remainingText = remainingText;
    }

    public Boolean getMemberOnly() {
        return isMemberOnly;
    }

    public void setMemberOnly(Boolean memberOnly) {
        isMemberOnly = memberOnly;
    }

    public Integer getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(Integer repeatType) {
        this.repeatType = repeatType;
    }

    public Integer getRepeatLimit() {
        return repeatLimit;
    }

    public void setRepeatLimit(Integer repeatLimit) {
        this.repeatLimit = repeatLimit;
    }

    public Boolean getClientCancel() {
        return isClientCancel;
    }

    public void setClientCancel(Boolean clientCancel) {
        isClientCancel = clientCancel;
    }

    public Integer getClientCancelDay() {
        return clientCancelDay;
    }

    public void setClientCancelDay(Integer clientCancelDay) {
        this.clientCancelDay = clientCancelDay;
    }

    public Boolean getReportQRCode() {
        return reportQRCode;
    }

    public void setReportQRCode(Boolean reportQRCode) {
        this.reportQRCode = reportQRCode;
    }

    public Boolean getReportWeb() {
        return reportWeb;
    }

    public void setReportWeb(Boolean reportWeb) {
        this.reportWeb = reportWeb;
    }

    public Boolean getSignUpLive() {
        return signUpLive;
    }

    public void setSignUpLive(Boolean signUpLive) {
        this.signUpLive = signUpLive;
    }

    public Long getReportField() {
        return reportField;
    }

    public void setReportField(Long reportField) {
        this.reportField = reportField;
    }

    public Boolean getReportConsiderations() {
        return isReportConsiderations;
    }

    public void setReportConsiderations(String reportConsiderations) {
        this.reportConsiderations = reportConsiderations;
    }

    public String getReportBackground() {
        return reportBackground;
    }

    public void setReportBackground(String reportBackground) {
        this.reportBackground = reportBackground;
    }

    public Timestamp getReportStart() {
        return reportStart;
    }

    public void setReportStart(Timestamp reportStart) {
        this.reportStart = reportStart;
    }

    public Timestamp getReportEnd() {
        return reportEnd;
    }

    public void setReportEnd(Timestamp reportEnd) {
        this.reportEnd = reportEnd;
    }

    public Boolean getReportPlace() {
        return isReportPlace;
    }

    public void setReportPlace(Boolean reportPlace) {
        isReportPlace = reportPlace;
    }

    public String getReportPlaceName() {
        return reportPlaceName;
    }

    public void setReportPlaceName(String reportPlaceName) {
        this.reportPlaceName = reportPlaceName;
    }

    public Integer getReportPlaceCountry() {
        return reportPlaceCountry;
    }

    public void setReportPlaceCountry(Integer reportPlaceCountry) {
        this.reportPlaceCountry = reportPlaceCountry;
    }

    public Integer getReportPlaceProvince() {
        return reportPlaceProvince;
    }

    public void setReportPlaceProvince(Integer reportPlaceProvince) {
        this.reportPlaceProvince = reportPlaceProvince;
    }

    public Integer getReportPlaceCity() {
        return reportPlaceCity;
    }

    public void setReportPlaceCity(Integer reportPlaceCity) {
        this.reportPlaceCity = reportPlaceCity;
    }

    public Integer getReportPlaceRegion() {
        return reportPlaceRegion;
    }

    public void setReportPlaceRegion(Integer reportPlaceRegion) {
        this.reportPlaceRegion = reportPlaceRegion;
    }

    public String getReportPlaceAddress() {
        return reportPlaceAddress;
    }

    public void setReportPlaceAddress(String reportPlaceAddress) {
        this.reportPlaceAddress = reportPlaceAddress;
    }

    public Boolean getClause() {
        return isClause;
    }

    public void setClause(String clause) {
        this.clause = clause;
    }

    public Boolean getVerificationCode() {
        return isVerificationCode;
    }

    public void setVerificationCode(Boolean verificationCode) {
        isVerificationCode = verificationCode;
    }

    public Boolean getNotify() {
        return isNotify;
    }

    public void setNotify(Boolean notify) {
        isNotify = notify;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getCardNo() {
        return cardNo;
    }

    public void setCardNo(Long cardNo) {
        this.cardNo = cardNo;
    }

    public String getNotifyManagement() {
        return notifyManagement;
    }

    public void setNotifyManagement(String notifyManagement) {
        this.notifyManagement = notifyManagement;
    }

    public Integer getQuestionnairesType() {
        return questionnairesType;
    }

    public void setQuestionnairesType(Integer questionnairesType) {
        this.questionnairesType = questionnairesType;
    }

    public String getQuestionnairesButtonText() {
        return questionnairesButtonText;
    }

    public void setQuestionnairesButtonText(String questionnairesButtonText) {
        this.questionnairesButtonText = questionnairesButtonText;
    }

    public Timestamp getQuestionnairesStart() {
        return questionnairesStart;
    }

    public void setQuestionnairesStart(Timestamp questionnairesStart) {
        this.questionnairesStart = questionnairesStart;
    }

    public Timestamp getQuestionnairesEnd() {
        return questionnairesEnd;
    }

    public void setQuestionnairesEnd(Timestamp questionnairesEnd) {
        this.questionnairesEnd = questionnairesEnd;
    }

    public String getQuestionnairesUrl() {
        return questionnairesUrl;
    }

    public void setQuestionnairesUrl(String questionnairesUrl) {
        this.questionnairesUrl = questionnairesUrl;
    }

    public Boolean getIndependent() {
        return independent;
    }

    public void setIndependent(Boolean independent) {
        this.independent = independent;
    }

    public Integer getIndependentType() {
        return independentType;
    }

    public void setIndependentType(Integer independentType) {
        this.independentType = independentType;
    }

    public String getIndependentLogo() {
        return independentLogo;
    }

    public void setIndependentLogo(String independentLogo) {
        this.independentLogo = independentLogo;
    }

    public String getIndependentLogoM() {
        return independentLogoM;
    }

    public void setIndependentLogoM(String independentLogoM) {
        this.independentLogoM = independentLogoM;
    }

    public String getIndependentImg() {
        return independentImg;
    }

    public void setIndependentImg(String independentImg) {
        this.independentImg = independentImg;
    }

    public String getIndependentImgM() {
        return independentImgM;
    }

    public void setIndependentImgM(String independentImgM) {
        this.independentImgM = independentImgM;
    }

    public Long getQuestionnaireMenuID() {
        return questionnaireMenuID;
    }

    public void setQuestionnaireMenuID(Long questionnaireMenuID) {
        this.questionnaireMenuID = questionnaireMenuID;
    }

    public Long getQuestionnaireFormID() {
        return questionnaireFormID;
    }

    public void setQuestionnaireFormID(Long questionnaireFormID) {
        this.questionnaireFormID = questionnaireFormID;
    }

    public void setClause(Boolean clause) {
        isClause = clause;
    }

    public void setReportConsiderations(Boolean reportConsiderations) {
        isReportConsiderations = reportConsiderations;
    }

    public void setReserveQuota(Boolean reserveQuota) {
        isReserveQuota = reserveQuota;
    }

    public void setWaitingQuota(Boolean waitingQuota) {
        isWaitingQuota = waitingQuota;
    }
}
