package com.example.demo.domain;

import org.springframework.context.annotation.Description;

import javax.persistence.*;

@Entity
@Table(name = "[Group]")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Icon")
    private String icon;

    @Column(name = "Color")
    private String color;

    @Column(name = "[Desc]", updatable = false)
    private String desc;

    @Column(name = "Status")
    private Boolean status;

    public Group() {
    }

    public Group(String name, String icon, String color, String desc, Boolean status) {
        this.name = name;
        this.icon = icon;
        this.color = color;
        this.desc = desc;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
