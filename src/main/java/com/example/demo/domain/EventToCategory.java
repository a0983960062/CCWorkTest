package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToCategory")
public class EventToCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "CategoryID")
    private Integer categoryID;

    @Column(name = "CategoryType")
    private String categoryType;

    public EventToCategory() {
    }

    public EventToCategory(Integer categoryID, String categoryType) {
        this.categoryID = categoryID;
        this.categoryType = categoryType;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }
}
