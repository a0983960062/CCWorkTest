package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToSite")
public class EventToSite {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "SiteID")
    private Integer siteID;

    public EventToSite() {
    }

    public EventToSite(Integer siteID) {
        this.siteID = siteID;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getSiteID() {
        return siteID;
    }

    public void setSiteID(Integer siteID) {
        this.siteID = siteID;
    }
}
