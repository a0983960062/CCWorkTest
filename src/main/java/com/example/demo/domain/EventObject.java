package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventObject")
public class EventObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "MenuID")
    private Long menuID;

    @Column(name = "Name")
    private String name;

    @Column(name = "Attention")
    private String attention;

    @Column(name = "IsIssue")
    private Boolean isIssue;

    @Column(name = "Sort")
    private Integer sort;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    public EventObject() {
    }

    public EventObject(Long menuID, String name, String attention, Boolean isIssue, Integer sort, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime) {
        this.menuID = menuID;
        this.name = name;
        this.attention = attention;
        this.isIssue = isIssue;
        this.sort = sort;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getMenuID() {
        return menuID;
    }

    public void setMenuID(Long menuID) {
        this.menuID = menuID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public Boolean getIssue() {
        return isIssue;
    }

    public void setIssue(Boolean issue) {
        isIssue = issue;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }
}
