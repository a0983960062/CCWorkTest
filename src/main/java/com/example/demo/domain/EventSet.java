package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "[EventSet]")
public class EventSet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CardNo")
    private Long cardNo;

    @Column(name = "Menus")
    private String menus;

    @Column(name = "Types")
    private String types;

    @Column(name = "PagingMode")
    private String pagingMode;

    @Column(name = "PageSize")
    private Integer pageSize;

    @Column(name = "IssueSetting")
    private String issueSetting;

    @Column(name = "SortMode")
    private String sortMode;

    @Column(name = "SortField")
    private String sortField;

    @Column(name = "DefaultImg")
    private String defaultImg;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    public EventSet() {
    }

    public EventSet(Long cardNo, String menus, String types, String pagingMode, Integer pageSize, String issueSetting, String sortMode, String sortField, String defaultImg, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime) {
        this.cardNo = cardNo;
        this.menus = menus;
        this.types = types;
        this.pagingMode = pagingMode;
        this.pageSize = pageSize;
        this.issueSetting = issueSetting;
        this.sortMode = sortMode;
        this.sortField = sortField;
        this.defaultImg = defaultImg;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
    }

    public Long getCardNo() {
        return cardNo;
    }

    public void setCardNo(Long cardNo) {
        this.cardNo = cardNo;
    }

    public String getMenus() {
        return menus;
    }

    public void setMenus(String menus) {
        this.menus = menus;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getPagingMode() {
        return pagingMode;
    }

    public void setPagingMode(String pagingMode) {
        this.pagingMode = pagingMode;
    }

    public Integer getPagingSize() {
        return pageSize;
    }

    public void setPagingSize(Integer pagingSize) {
        this.pageSize = pagingSize;
    }

    public String getIssueSetting() {
        return issueSetting;
    }

    public void setIssueSetting(String issueSetting) {
        this.issueSetting = issueSetting;
    }

    public String getSortMode() {
        return sortMode;
    }

    public void setSortMode(String sortMode) {
        this.sortMode = sortMode;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getDefaultImg() {
        return defaultImg;
    }

    public void setDefaultImg(String defaultImg) {
        this.defaultImg = defaultImg;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }
}
