package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventSetting")
public class EventSetting {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MenuID")
    private Integer menuID;

    @Column(name = "Types")
    private String types;

    @Column(name = "PagingMode")
    private String pagingMode;

    @Column(name = "PageSize")
    private Integer pageSize;

    @Column(name = "IssueSetting")
    private String issueSetting;

    @Column(name = "SortMode")
    private String sortMode;

    @Column(name = "SortField")
    private String sortField;

    @Column(name = "DefaultImg")
    private String defaultImg;

    @Column(name = "ReplyStatus")
    private String replyStatus;

    @Column(name = "ReplyTitle")
    private String replyTitle;

    @Column(name = "ReplyPageSize")
    private Integer replyPageSize;

    @Column(name = "ReplyFBID")
    private String replyFBID;

    @Column(name = "ReplyFBAccounts")
    private String replyFBAccounts;

    @Column(name = "ExtendReadOpen")
    private Boolean extendReadOpen;

    @Column(name = "ExtendReadTitle")
    private String extendReadTitle;

    @Column(name = "ExtendReadMenus")
    private String extendReadMenus;

    @Column(name = "ExtendReadMode")
    private Integer extendReadMode;

    @Column(name = "ADOpen")
    private Boolean aopOpen;

    @Column(name = "ADTitle")
    private String adTitle;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    public EventSetting() {
    }

    public EventSetting(String types, String pagingMode, Integer pageSize, String issueSetting, String sortMode, String sortField, String defaultImg, String replyStatus, String replyTitle, Integer replyPageSize, String replyFBID, String replyFBAccounts, Boolean extendReadOpen, String extendReadTitle, String extendReadMenus, Integer extendReadMode, Boolean aopOpen, String adTitle, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime) {
        this.types = types;
        this.pagingMode = pagingMode;
        this.pageSize = pageSize;
        this.issueSetting = issueSetting;
        this.sortMode = sortMode;
        this.sortField = sortField;
        this.defaultImg = defaultImg;
        this.replyStatus = replyStatus;
        this.replyTitle = replyTitle;
        this.replyPageSize = replyPageSize;
        this.replyFBID = replyFBID;
        this.replyFBAccounts = replyFBAccounts;
        this.extendReadOpen = extendReadOpen;
        this.extendReadTitle = extendReadTitle;
        this.extendReadMenus = extendReadMenus;
        this.extendReadMode = extendReadMode;
        this.aopOpen = aopOpen;
        this.adTitle = adTitle;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
    }

    public Integer getMenuID() {
        return menuID;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getPagingMode() {
        return pagingMode;
    }

    public void setPagingMode(String pagingMode) {
        this.pagingMode = pagingMode;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getIssueSetting() {
        return issueSetting;
    }

    public void setIssueSetting(String issueSetting) {
        this.issueSetting = issueSetting;
    }

    public String getSortMode() {
        return sortMode;
    }

    public void setSortMode(String sortMode) {
        this.sortMode = sortMode;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getDefaultImg() {
        return defaultImg;
    }

    public void setDefaultImg(String defaultImg) {
        this.defaultImg = defaultImg;
    }

    public String getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(String replyStatus) {
        this.replyStatus = replyStatus;
    }

    public String getReplyTitle() {
        return replyTitle;
    }

    public void setReplyTitle(String replyTitle) {
        this.replyTitle = replyTitle;
    }

    public Integer getReplyPageSize() {
        return replyPageSize;
    }

    public void setReplyPageSize(Integer replyPageSize) {
        this.replyPageSize = replyPageSize;
    }

    public String getReplyFBID() {
        return replyFBID;
    }

    public void setReplyFBID(String replyFBID) {
        this.replyFBID = replyFBID;
    }

    public String getReplyFBAccounts() {
        return replyFBAccounts;
    }

    public void setReplyFBAccounts(String replyFBAccounts) {
        this.replyFBAccounts = replyFBAccounts;
    }

    public Boolean getExtendReadOpen() {
        return extendReadOpen;
    }

    public void setExtendReadOpen(Boolean extendReadOpen) {
        this.extendReadOpen = extendReadOpen;
    }

    public String getExtendReadTitle() {
        return extendReadTitle;
    }

    public void setExtendReadTitle(String extendReadTitle) {
        this.extendReadTitle = extendReadTitle;
    }

    public String getExtendReadMenus() {
        return extendReadMenus;
    }

    public void setExtendReadMenus(String extendReadMenus) {
        this.extendReadMenus = extendReadMenus;
    }

    public Integer getExtendReadMode() {
        return extendReadMode;
    }

    public void setExtendReadMode(Integer extendReadMode) {
        this.extendReadMode = extendReadMode;
    }

    public Boolean getAopOpen() {
        return aopOpen;
    }

    public void setAopOpen(Boolean aopOpen) {
        this.aopOpen = aopOpen;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }
}
