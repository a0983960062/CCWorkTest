package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToYear")
public class EventToYear {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "YearID")
    private Integer yearID;

    @Column(name = "Sort")
    private Integer sort;

    public EventToYear() {
    }

    public EventToYear(Integer yearID, Integer sort) {
        this.yearID = yearID;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getYearID() {
        return yearID;
    }

    public void setYearID(Integer yearID) {
        this.yearID = yearID;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
