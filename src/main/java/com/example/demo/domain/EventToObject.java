package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToObject")
public class EventToObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "ObjectID")
    private Integer objectID;

    @Column(name = "Sort")
    private Integer sort;

    public EventToObject() {
    }

    public EventToObject(Integer objectID, Integer sort) {
        this.objectID = objectID;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getObjectID() {
        return objectID;
    }

    public void setObjectID(Integer objectID) {
        this.objectID = objectID;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
