package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventApplicantGuid")
public class EventApplicantGuid {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "Guid")
    private String guid;

    public EventApplicantGuid() {
    }

    public EventApplicantGuid(String guid) {
        this.guid = guid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
