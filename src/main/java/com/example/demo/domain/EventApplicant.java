package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventApplicant")
public class EventApplicant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "EventID")
    private Long eventID;

    @Column(name = "FrontEnd")
    private Boolean frontEnd;

    @Column(name = "Status")
    private Integer status;

    @Column(name = "CheckIn")
    private Boolean checkIn;

    @Column(name = "SignUpTime")
    private Timestamp signUpTime;

    @Column(name = "CheckInTime")
    private Timestamp checkInTime;

    @Column(name = "CancelTime")
    private Timestamp cancelTime;

    @Column(name = "Remark")
    private String remark;

    public EventApplicant() {
    }

    public EventApplicant(Long eventID, Boolean frontEnd, Integer status, Boolean checkIn, Timestamp signUpTime, Timestamp checkInTime, Timestamp cancelTime, String remark) {
        this.eventID = eventID;
        this.frontEnd = frontEnd;
        this.status = status;
        this.checkIn = checkIn;
        this.signUpTime = signUpTime;
        this.checkInTime = checkInTime;
        this.cancelTime = cancelTime;
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getEventID() {
        return eventID;
    }

    public void setEventID(Long eventID) {
        this.eventID = eventID;
    }

    public Boolean getFrontEnd() {
        return frontEnd;
    }

    public void setFrontEnd(Boolean frontEnd) {
        this.frontEnd = frontEnd;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Boolean checkIn) {
        this.checkIn = checkIn;
    }

    public Timestamp getSignUpTime() {
        return signUpTime;
    }

    public void setSignUpTime(Timestamp signUpTime) {
        this.signUpTime = signUpTime;
    }

    public Timestamp getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Timestamp checkInTime) {
        this.checkInTime = checkInTime;
    }

    public Timestamp getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Timestamp cancelTime) {
        this.cancelTime = cancelTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
