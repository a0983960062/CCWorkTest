package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "[Events]")
public class Events {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "SiteID")
    private Long sitrID;

    @Column(name = "MenuID")
    private Long menuID;

    @Column(name = "CardNo")
    private Long cardNo;

    @Column(name = "Title")
    private String title;

    @Column(name = "IssueDate")
    private Timestamp issueDate;

    @Column(name = "RemarkText")
    private String remarkText;

    @Column(name = "CustomIcon")
    private String customIcon;

    @Column(name = "Icon")
    private String icon;

    @Column(name = "DateKind")
    private String dateKind;

    @Column(name = "DateStart")
    private Timestamp dateStart;

    @Column(name = "DateEnd")
    private Timestamp dateEnd;

    @Column(name = "TimeStart")
    private String timeStart;

    @Column(name = "TimeEnd")
    private String timeEnd;

    @Column(name = "TimeDesc")
    private String timeDesc;

    @Column(name = "SignUpForceEnd")
    private Boolean signUpForceEnd;

    @Column(name = "IsIssue")
    private Boolean isIssue;

    @Column(name = "IssueStart")
    private Timestamp issueStart;

    @Column(name = "IssueEnd")
    private Timestamp issueEnd;

    @Column(name = "Sort")
    private Integer sort;

    @Column(name = "Clicks")
    private Integer clicks;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    @Column(name = "TmplName")
    private String tmplName;

    @Column(name = "TmplThumb")
    private String tmplThumb;

    @Column(name = "DayDiff")
    private String dayDiff;

    public Events() {
    }

    public Events(Long sitrID, Long menuID, Long cardNo, String title, Timestamp issueDate, String remarkText, String customIcon, String icon, String dateKind, Timestamp dateStart, Timestamp dateEnd, String timeStart, String timeEnd, String timeDesc, Boolean signUpForceEnd, Boolean isIssue, Timestamp issueStart, Timestamp issueEnd, Integer sort, Integer clicks, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime, String tmplName, String tmplThumb, String dayDiff) {
        this.sitrID = sitrID;
        this.menuID = menuID;
        this.cardNo = cardNo;
        this.title = title;
        this.issueDate = issueDate;
        this.remarkText = remarkText;
        this.customIcon = customIcon;
        this.icon = icon;
        this.dateKind = dateKind;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.timeDesc = timeDesc;
        this.signUpForceEnd = signUpForceEnd;
        this.isIssue = isIssue;
        this.issueStart = issueStart;
        this.issueEnd = issueEnd;
        this.sort = sort;
        this.clicks = clicks;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
        this.tmplName = tmplName;
        this.tmplThumb = tmplThumb;
        this.dayDiff = dayDiff;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getSitrID() {
        return sitrID;
    }

    public void setSitrID(Long sitrID) {
        this.sitrID = sitrID;
    }

    public Long getMenuID() {
        return menuID;
    }

    public void setMenuID(Long menuID) {
        this.menuID = menuID;
    }

    public Long getCardNo() {
        return cardNo;
    }

    public void setCardNo(Long cardNo) {
        this.cardNo = cardNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Timestamp issueDate) {
        this.issueDate = issueDate;
    }

    public String getRemarkText() {
        return remarkText;
    }

    public void setRemarkText(String remarkText) {
        this.remarkText = remarkText;
    }

    public String getCustomIcon() {
        return customIcon;
    }

    public void setCustomIcon(String customIcon) {
        this.customIcon = customIcon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDateKind() {
        return dateKind;
    }

    public void setDateKind(String dateKind) {
        this.dateKind = dateKind;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTimeDesc() {
        return timeDesc;
    }

    public void setTimeDesc(String timeDesc) {
        this.timeDesc = timeDesc;
    }

    public Boolean getSignUpForceEnd() {
        return signUpForceEnd;
    }

    public void setSignUpForceEnd(Boolean signUpForceEnd) {
        this.signUpForceEnd = signUpForceEnd;
    }

    public Boolean getIssue() {
        return isIssue;
    }

    public void setIssue(Boolean issue) {
        isIssue = issue;
    }

    public Timestamp getIssueStart() {
        return issueStart;
    }

    public void setIssueStart(Timestamp issueStart) {
        this.issueStart = issueStart;
    }

    public Timestamp getIssueEnd() {
        return issueEnd;
    }

    public void setIssueEnd(Timestamp issueEnd) {
        this.issueEnd = issueEnd;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getTmplName() {
        return tmplName;
    }

    public void setTmplName(String tmplName) {
        this.tmplName = tmplName;
    }

    public String getTmplThumb() {
        return tmplThumb;
    }

    public void setTmplThumb(String tmplThumb) {
        this.tmplThumb = tmplThumb;
    }

    public String getDayDiff() {
        return dayDiff;
    }

    public void setDayDiff(String dayDiff) {
        this.dayDiff = dayDiff;
    }
}
