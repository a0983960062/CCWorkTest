package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventGuest")
public class EventGuest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "MenuID")
    private Long menuID;

    @Column(name = "Name")
    private String name;

    @Column(name = "Title")
    private String title;

    @Column(name = "Photo")
    private String photo;

    @Column(name = "BriefIntro")
    private String briefIntro;

    @Column(name = "Intro")
    private String intro;

    @Column(name = "Resume")
    private String resume;

    @Column(name = "Website")
    private String website;

    @Column(name = "IsIssue")
    private Boolean isIssue;

    @Column(name = "Sort")
    private Integer sort;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    @Column(name = "Phone")
    private String phone;

    @Column(name = "Fax")
    private String fax;

    @Column(name = "Email")
    private String email;

    public EventGuest() {
    }

    public EventGuest(Long menuID, String name, String title, String photo, String briefIntro, String intro, String resume, String website, Boolean isIssue, Integer sort, Long creator, Timestamp createTime, Long modifier, Timestamp modifyTime, String phone, String fax, String email) {
        this.menuID = menuID;
        this.name = name;
        this.title = title;
        this.photo = photo;
        this.briefIntro = briefIntro;
        this.intro = intro;
        this.resume = resume;
        this.website = website;
        this.isIssue = isIssue;
        this.sort = sort;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getMenuID() {
        return menuID;
    }

    public void setMenuID(Long menuID) {
        this.menuID = menuID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBriefIntro() {
        return briefIntro;
    }

    public void setBriefIntro(String briefIntro) {
        this.briefIntro = briefIntro;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Boolean getIssue() {
        return isIssue;
    }

    public void setIssue(Boolean issue) {
        isIssue = issue;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
