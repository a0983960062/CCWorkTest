package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToPlace")
public class EventToPlace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "PlaceID")
    private Integer placeID;

    @Column(name = "Sort")
    private Integer sort;

    public EventToPlace() {
    }

    public EventToPlace(Integer placeID, Integer sort) {
        this.placeID = placeID;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getPlaceID() {
        return placeID;
    }

    public void setPlaceID(Integer placeID) {
        this.placeID = placeID;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
