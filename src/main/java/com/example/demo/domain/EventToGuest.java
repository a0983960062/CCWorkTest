package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToGuest")
public class EventToGuest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "GuestID")
    private Integer guestID;

    @Column(name = "Kind")
    private String kind;

    @Column(name = "Sort")
    private Integer sort;

    public EventToGuest() {
    }

    public EventToGuest(Integer guestID, String kind, Integer sort) {
        this.guestID = guestID;
        this.kind = kind;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getGuestID() {
        return guestID;
    }

    public void setGuestID(Integer guestID) {
        this.guestID = guestID;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
