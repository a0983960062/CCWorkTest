package com.example.demo.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "EventSignUpSendMailLog")
public class EventSignUpSendMailLog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "MailTitle")
    private String mailTitle;

    @Column(name = "MailContent")
    private String mailContent;

    @Column(name = "Sender")
    private String sender;

    @Column(name = "SenderMail")
    private String senderMail;

    @Column(name = "Attachments")
    private String attachments;

    @Column(name = "ApplicantIDs")
    private String applicantIDs;

    @Column(name = "Creator")
    private Long creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    public EventSignUpSendMailLog() {
    }

    public EventSignUpSendMailLog(Integer eventID, String mailTitle, String mailContent, String sender, String senderMail, String attachments, String applicantIDs, Long creator, Timestamp createTime) {
        this.eventID = eventID;
        this.mailTitle = mailTitle;
        this.mailContent = mailContent;
        this.sender = sender;
        this.senderMail = senderMail;
        this.attachments = attachments;
        this.applicantIDs = applicantIDs;
        this.creator = creator;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public String getMailTitle() {
        return mailTitle;
    }

    public void setMailTitle(String mailTitle) {
        this.mailTitle = mailTitle;
    }

    public String getMailContent() {
        return mailContent;
    }

    public void setMailContent(String mailContent) {
        this.mailContent = mailContent;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderMail() {
        return senderMail;
    }

    public void setSenderMail(String senderMail) {
        this.senderMail = senderMail;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public String getApplicantIDs() {
        return applicantIDs;
    }

    public void setApplicantIDs(String applicantIDs) {
        this.applicantIDs = applicantIDs;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}
