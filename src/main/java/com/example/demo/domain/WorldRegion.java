package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "WorldRegion")
public class WorldRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "ParentID")
    private Long parentId;

    @Column(name = "Levels")
    private Integer levels;

    @Column(name = "Name")
    private String name;

    @Column(name = "Sort")
    private Integer sort;

    public WorldRegion() {
    }

    public WorldRegion(Long parentId, Integer levels, String name, Integer sort) {
        this.parentId = parentId;
        this.levels = levels;
        this.name = name;
        this.sort = sort;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getLevels() {
        return levels;
    }

    public void setLevels(Integer levels) {
        this.levels = levels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
