package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToOrganizer")
public class EventToOrganizer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "OrganizerID")
    private Integer organizerID;

    @Column(name = "Sort")
    private Integer sort;

    public EventToOrganizer() {
    }

    public EventToOrganizer(Integer organizerID, Integer sort) {
        this.organizerID = organizerID;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getOrganizerID() {
        return organizerID;
    }

    public void setOrganizerID(Integer organizerID) {
        this.organizerID = organizerID;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
