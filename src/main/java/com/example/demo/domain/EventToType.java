package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToType")
public class EventToType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "TypeID")
    private Integer typeID;

    @Column(name = "Sort")
    private Integer sort;

    public EventToType() {
    }

    public EventToType(Integer typeID, Integer sort) {
        this.typeID = typeID;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getTypeID() {
        return typeID;
    }

    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
