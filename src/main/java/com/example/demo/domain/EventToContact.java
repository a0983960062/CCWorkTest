package com.example.demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "EventToContact")
public class EventToContact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EventID")
    private Integer eventID;

    @Column(name = "ContactID")
    private Integer contactID;

    @Column(name = "Sort")
    private Integer sort;

    public EventToContact() {
    }

    public EventToContact(Integer contactID, Integer sort) {
        this.contactID = contactID;
        this.sort = sort;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public Integer getContactID() {
        return contactID;
    }

    public void setContactID(Integer contactID) {
        this.contactID = contactID;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
