package com.example.demo.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessorOrder;
import java.sql.Timestamp;

@Entity
@Table(name = "MemberShip")
public class MemberShip {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "SiteID")
    private Integer siteID;

    @Column(name = "Account")
    private String account;

    @Column(name = "Password")
    private String password;

    @Column(name = "Name")
    private String name;

    @Column(name = "Sex")
    private String sex;

    @Column(name = "IDCard")
    private String idCard;

    @Column(name = "Birthday")
    private Timestamp birthday;

    @Column(name = "Phone")
    private String phone;

    @Column(name = "Mobile")
    private String mobile;

    @Column(name = "Email")
    private String email;

    @Column(name = "Status")
    private Boolean status;

    @Column(name = "LastLoginTime")
    private Timestamp lastLoginTime;

    @Column(name = "Creator")
    private Integer creator;

    @Column(name = "CreateTime")
    private Timestamp createTime;

    @Column(name = "Modifier")
    private Long modifier;

    @Column(name = "ModifyTime")
    private Timestamp modifyTime;

    @Column(name = "VerifyType")
    private Integer verifyType;

    @Column(name = "VerifyCode")
    private String verifyCode;

    @Column(name = "VerifyTime")
    private String verifyTime;

    @Column(name = "ResetPWD")
    private Boolean resetPWD;

    @Column(name = "MemberType")
    private Integer memberType;

    @Column(name = "SocialID")
    private String socialID;

    @Column(name = "LastResentVerifyMailTime")
    private String lastResentVerifyMailTime;

    @Column(name = "RecommandCode")
    private String recommandCode;

    @Column(name = "PushRecommandCode")
    private String pushRecommandCode;

    public MemberShip() {
    }

    public MemberShip(Integer siteID, String account, String password, String name, String sex, String idCard, Timestamp birthday, String phone, String mobile, String email, Boolean status, Timestamp lastLoginTime, Integer creator, Timestamp createTime, Long modifier, Timestamp modifyTime, Integer verifyType, String verifyCode, String verifyTime, Boolean resetPWD, Integer memberType, String socialID, String lastResentVerifyMailTime, String recommandCode, String pushRecommandCode) {
        this.siteID = siteID;
        this.account = account;
        this.password = password;
        this.name = name;
        this.sex = sex;
        this.idCard = idCard;
        this.birthday = birthday;
        this.phone = phone;
        this.mobile = mobile;
        this.email = email;
        this.status = status;
        this.lastLoginTime = lastLoginTime;
        this.creator = creator;
        this.createTime = createTime;
        this.modifier = modifier;
        this.modifyTime = modifyTime;
        this.verifyType = verifyType;
        this.verifyCode = verifyCode;
        this.verifyTime = verifyTime;
        this.resetPWD = resetPWD;
        this.memberType = memberType;
        this.socialID = socialID;
        this.lastResentVerifyMailTime = lastResentVerifyMailTime;
        this.recommandCode = recommandCode;
        this.pushRecommandCode = pushRecommandCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSiteID() {
        return siteID;
    }

    public void setSiteID(Integer siteID) {
        this.siteID = siteID;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getVerifyType() {
        return verifyType;
    }

    public void setVerifyType(Integer verifyType) {
        this.verifyType = verifyType;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(String verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Boolean getResetPWD() {
        return resetPWD;
    }

    public void setResetPWD(Boolean resetPWD) {
        this.resetPWD = resetPWD;
    }

    public Integer getMemberType() {
        return memberType;
    }

    public void setMemberType(Integer memberType) {
        this.memberType = memberType;
    }

    public String getSocialID() {
        return socialID;
    }

    public void setSocialID(String socialID) {
        this.socialID = socialID;
    }

    public String getLastResentVerifyMailTime() {
        return lastResentVerifyMailTime;
    }

    public void setLastResentVerifyMailTime(String lastResentVerifyMailTime) {
        this.lastResentVerifyMailTime = lastResentVerifyMailTime;
    }

    public String getRecommandCode() {
        return recommandCode;
    }

    public void setRecommandCode(String recommandCode) {
        this.recommandCode = recommandCode;
    }

    public String getPushRecommandCode() {
        return pushRecommandCode;
    }

    public void setPushRecommandCode(String pushRecommandCode) {
        this.pushRecommandCode = pushRecommandCode;
    }
}
